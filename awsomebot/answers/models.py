from django.db import models


class Answers(models.Model):

    answer_tag = models.CharField(max_length=50)
    answer_text = models.CharField(max_length=200)
    added_date = models.DateTimeField('date added')

    def __str__(self):
        return '{} : {}'.format(self.answer_tag, self.answer_text)
# Create your models here.
