# Create your views here.
from django.http import JsonResponse
from .models import Answers

# ILL HAVE TO RETURN JSON INSTEAD OF A RENDER


def index(request):
    tag = request.GET.get('tag', '')
    answer = Answers.objects.get(answer_tag=tag)
    print(answer)
    return JsonResponse({"answer": answer.answer_text})
