function addMessageToHistory(message){
    const history = document.querySelector('.history')
    history.innerHTML += '<br> <br> <br>' + message
}

const button = document.querySelector('.send')
button.addEventListener('click', function(){
    const message = document.querySelector('.message')
    addMessageToHistory(`USER ${message.value}`)
    getAnswerFromBot(message.value)
    message.value = ''
    
})

const message = document.querySelector('.message');
message.addEventListener("keydown", function(event) {
  const text = message.value;
  const enterKey = 13;
  if (event.which == enterKey) {
    addMessageToHistory(`USER: ${text}`)
    getAnswerFromBot(message.value)
    message.value = ''
    event.preventDefault();
    
  }  
  
});

async function getAnswerFromBot(sentence){
    let answer = await fetch('http://localhost:8080',{method:'POST',body:JSON.stringify({sentence})})
    answer = await answer.json()
    addMessageToHistory(`BOT: ${answer.answer}`)
}