import requests


class DService(object):
    DJANGO_URL = 'http://localhost:8081/answers/'

    def get(self, tag):
        internal_url = '{}?tag={}'.format(self.DJANGO_URL, tag)
        result = requests.get(internal_url)
        return result.json()

# USUALLY I would use requests as a collaborator, but I want to show I can be flexible
