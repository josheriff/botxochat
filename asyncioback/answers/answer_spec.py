from mamba import describe, context, it
from expects import expect, be_empty
from doublex import Stub, when, Spy
from doublex_expects import have_been_called_with

from asyncioback.answers.answers import Answer

with describe('given a sentence'):
    with context('use the tag ia service'):
        with it('uses it'):
            a_sentence = 'hello, how are you today?'
            ia_tagger = Spy()
            django_service = Stub()

            answer = Answer(ia_tagger, django_service)
            answer.get_answer(a_sentence)

            expect(ia_tagger.send).to(have_been_called_with(a_sentence))

    with context('send the tag to django service'):  # bad description, but for clarify in exam
        with it('django service its used'):
            a_sentence = 'hello, how are you today?'
            ia_tagger = Stub()
            django_service = Spy()
            when(ia_tagger).send(a_sentence).returns('a_tag')

            answer = Answer(ia_tagger, django_service)
            answer.get_answer(a_sentence)

            expect(django_service.get).to(have_been_called_with('a_tag'))

        with context('send sentence to AI service'):
            with it('returns any tag'):
                a_sentence = 'hello, how are you today?'
                response_sentence = 'Fine, thx'
                ia_tagger = Stub()
                django_service = Stub()
                when(ia_tagger).send(a_sentence).returns('a_tag')
                when(django_service).get('a_tag').returns(response_sentence)

                answer = Answer(ia_tagger, django_service)
                result = answer.get_answer(a_sentence)

                expect(result).not_to(be_empty)


