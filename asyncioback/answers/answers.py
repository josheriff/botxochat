class Answer(object):
    def __init__(self, ia_tagger, django_service):
        self.ia_tagger = ia_tagger
        self.django_service = django_service

    def get_answer(self, sentence):
        tag = self._tag_sentence(sentence)
        return self.django_service.get(tag)

    def _tag_sentence(self, sentence):
        return self.ia_tagger.send(sentence)
