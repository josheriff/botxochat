# this service is not a trully service
# it mocks the behaivour of an IA sentence tagger


class Tagger(object):
    def send(self, sentence):
        sentence = sentence.upper()
        if 'BROKEN' in sentence:
            return 'broken_parcel'
        if 'DIRECTION' in sentence:
            return 'wrong_direction'
        if 'LATE' in sentence or 'YET' in sentence:
            return 'parcel_late'
        return 'random'
