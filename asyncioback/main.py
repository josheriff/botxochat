import asyncio
from aiohttp import web
import aiohttp_cors

from asyncioback.iatagger.tagger import Tagger
from asyncioback.djangoservice.service import DService
from asyncioback.answers.answers import Answer

ia_tagger = Tagger()
django_service = DService()
answer = Answer(ia_tagger, django_service)

async def get_answer(request):
    data = await request.json()
    sentence = data.get('sentence')
    response = answer.get_answer(sentence)
    return web.json_response(response)


app = web.Application()
cors = aiohttp_cors.setup(app)
resource = cors.add(app.router.add_resource('/'))
route = cors.add(
    resource.add_route("POST", get_answer), {
        "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers=("X-Custom-Server-Header",),
            allow_headers=("X-Requested-With", "Content-Type"),
            max_age=3600,
        )
    })
# cors.add(route, {
#         "*":
#             aiohttp_cors.ResourceOptions(allow_credentials=True)
#     })
# app.router.add_route('POST', '/', get_answer)

loop = asyncio.get_event_loop()
f = loop.create_server(app.make_handler(), '0.0.0.0', 8080)
srv = loop.run_until_complete(f)
print('serving on', srv.sockets[0].getsockname())
try:
    loop.run_forever()
except KeyboardInterrupt:
    pass
