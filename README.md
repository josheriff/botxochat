# Botxo chat

- En el momento de esta documentación no se tiene acceso a un ordenador con docker, todo programado desde c9.io
    - DockersFiles creados pero no se han podido testear.
- instalar virtualenv en la raiz del proyecto
- ``pip install -r requirements.txt``

- Necesitarás 2 terminales si los docker no funiconan:
    - Primer terminal ejecuta `PYTHONPATH=. python asyncioback/main.py`
    - Segundo ve a `cd awsomebot` y ejecuta `python manage.py runserver 8081`
   
Puedes abrir directamente el html:

`front/index.html`


## Funcionamiento

- El front manda el texto al backend escrito en asyncio mediante metodo post.
- El backend en asyncio lo recibe, lo manda a un servicio de Inteligencia Artifical que le pone un ``tag`` 
para clasificar el problema (el servicio, se ha mockeado se encuentra en ``asyncioback/iatagger`)
    - Se ha hecho así por como se entendió el flujo en la primera entrevista, se entendía que había un servicio que decidía 
    la semantica de la frase enviada por el humano.
- Una vez tenemos el tag, se envía al backend Django, mediante metodo GET, este consulta en la base de datos y contesta la frase asociada.
- Todo vuelve al front donde se muestra.
- Para poder modificar la base de datos, ir a la url de django `localhost:8081/admin` con 
usuario: `jose` password:`botxomolamucho

## Decisiones fuera de lo pedido:

- Se usa sqlite3, dado que en el momento de realización de la prueba no se conoce Django lo suficiente como para añdir un comportamiento de ese estilo
- Se usa un "servicio de terceros" para clasificar las frases según tag, porque de los modelos habituales para decidir semantica
de la frase parecía el más sencillo, dejando además una base de datos más sencilla.
- Se usa como framework de testing ``mamba`` por su estilo bdd y porque es de un amigo, además de sentirme muy comodo con él.
- No se testea, la magia de Django (incluida la librería para acceder a BBDD), ni de asyncio, ni tampoco el front(este último por falta de tiempo).
- Se testea por tanto solo la parte de lógica de asyncio.
- Se ha dejado un poco de lógica en la vista de Django, porque en todos los ejemplos encontrados se dejan ahí.
se desconoce la arquitectura habitual de una app Django, aunque, si de mi dependiera, habría añadido un controlador.
- He subido los archivos de BBDD por no contener ninguna información sensible
- Se deja gestionando la BBDD por la parte de Django, desde la ruta admin, sospecho que se puede hacer algo mejor, definiendo usuarios o grupos, pero no hay tiempo para investigar.

## No da tiempo, o imposibilitado en este momento.

- No he podido meterlo todo en docker, porque la máquina con la que suelo trabajar está estropeada,
he tenido que usar otra y no podía instalar nada, se consigue programar mediante c9.io (IDE en la nube)
- No he realizado el random, aunque tengo perfectamente claro como hacerlo (obteniendo todas las frases y cogiendo una al azar).
